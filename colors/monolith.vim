" vim: set nowrap:

" HEADER

" Description:  Vim color scheme, monolith.vim
" Author:       dsjkvf (dsjkvf@gmail.com)
" Maintainer:   dsjkvf (dsjkvf@gmail.com)
" Webpage:      https://bitbucket.org/dsjkvf/vim-colors-monolith
" License:      MIT
" Version:      0.3

" MAIN

" Checkup
if !has('gui_running') && &t_Co < 256
    echom 'WARNING: The colorscheme is not supported by this terminal'
    finish
endif

" Startup
hi clear
if exists("syntax_on")
    syntax reset
endif
let colors_name = "monolith"

" Settings
" check if the terminal declares support for italic
if &term =~# 'italic'
    let g:monochrome_italic = 'italic'
else
    let g:monochrome_italic = 'NONE'
endif

" Colours
" core
hi Normal           cterm=NONE      ctermbg=000       ctermfg=250       gui=NONE      guibg=#000000   guifg=#bcbcbc
set background=dark

" core interface
hi ColorColumn      cterm=NONE      ctermbg=236       ctermfg=NONE      gui=NONE      guibg=#303030   guifg=NONE
hi Conceal          cterm=NONE      ctermbg=NONE      ctermfg=bg        gui=NONE      guibg=NONE      guifg=bg
hi Cursor           cterm=NONE      ctermbg=fg        ctermfg=bg        gui=NONE      guibg=fg        guifg=bg
hi CursorColumn     cterm=NONE      ctermbg=240       ctermfg=NONE      gui=NONE      guibg=#585858   guifg=NONE
hi CursorLine       cterm=NONE      ctermbg=240       ctermfg=NONE      gui=NONE      guibg=#585858   guifg=NONE
hi CursorLineNr     cterm=bold      ctermbg=NONE      ctermfg=015       gui=bold      guibg=NONE      guifg=#ffffff
hi Directory        cterm=bold      ctermbg=NONE      ctermfg=015       gui=bold      guibg=NONE      guifg=#ffffff
hi ErrorMsg         cterm=NONE      ctermbg=196       ctermfg=015       gui=NONE      guibg=#ff0000   guifg=#ffffff
hi FoldColumn       cterm=NONE      ctermbg=NONE      ctermfg=248       gui=NONE      guibg=NONE      guifg=#a8a8a8
hi Folded           cterm=NONE      ctermbg=236       ctermfg=248       gui=NONE      guibg=#303030   guifg=#a8a8a8
hi LineNr           cterm=NONE      ctermbg=NONE      ctermfg=240       gui=NONE      guibg=NONE      guifg=#585858
hi MatchParen       cterm=NONE      ctermbg=NONE      ctermfg=015       gui=NONE      guibg=NONE      guifg=#ffffff
hi ModeMsg          cterm=NONE      ctermbg=NONE      ctermfg=015       gui=NONE      guibg=NONE      guifg=#ffffff
hi MoreMsg          cterm=NONE      ctermbg=NONE      ctermfg=069       gui=NONE      guibg=NONE      guifg=#5f87ff
hi NonText          cterm=NONE      ctermbg=NONE      ctermfg=bg        gui=NONE      guibg=NONE      guifg=bg
hi Pmenu            cterm=NONE      ctermbg=069       ctermfg=015       gui=NONE      guibg=#5f87ff   guifg=#ffffff
hi PmenuSbar        cterm=NONE      ctermbg=015       ctermfg=015       gui=NONE      guibg=#ffffff   guifg=#ffffff
hi PmenuSel         cterm=NONE      ctermbg=015       ctermfg=069       gui=NONE      guibg=#ffffff   guifg=#5f87ff
hi PmenuThumb       cterm=NONE      ctermbg=069       ctermfg=069       gui=NONE      guibg=#5f87ff   guifg=#5f87ff
hi Question         cterm=NONE      ctermbg=NONE      ctermfg=121       gui=NONE      guibg=NONE      guifg=#87ffaf
hi Search           cterm=NONE      ctermbg=069       ctermfg=015       gui=NONE      guibg=#5f87ff   guifg=#ffffff
hi IncSearch        cterm=NONE      ctermbg=015       ctermfg=069       gui=NONE      guibg=#ffffff   guifg=#5f87ff
hi SignColumn       cterm=NONE      ctermbg=NONE      ctermfg=121       gui=NONE      guibg=NONE      guifg=#87ffaf
hi StatusLine       cterm=NONE      ctermbg=fg        ctermfg=bg        gui=NONE      guibg=fg        guifg=bg
hi StatusLineNC     cterm=NONE      ctermbg=240       ctermfg=bg        gui=NONE      guibg=#585858   guifg=bg
hi TabLine          cterm=NONE      ctermbg=240       ctermfg=bg        gui=NONE      guibg=#585858   guifg=bg
hi TabLineFill      cterm=NONE      ctermbg=240       ctermfg=bg        gui=NONE      guibg=#585858   guifg=bg
hi TabLineSel       cterm=NONE      ctermbg=fg        ctermfg=bg        gui=NONE      guibg=fg        guifg=bg
hi Title            cterm=bold      ctermbg=NONE      ctermfg=015       gui=bold      guibg=NONE      guifg=#ffffff
hi VertSplit        cterm=NONE      ctermbg=NONE      ctermfg=240       gui=NONE      guibg=NONE      guifg=#585858
hi Visual           cterm=NONE      ctermbg=153       ctermfg=bg        gui=NONE      guibg=#afd5ff   guifg=bg
hi WarningMsg       cterm=bold      ctermbg=NONE      ctermfg=196       gui=bold      guibg=NONE      guifg=#ff0000
hi WildMenu         cterm=NONE      ctermbg=069       ctermfg=bg        gui=NONE      guibg=#5f87ff   guifg=bg

" core development
hi Comment          cterm=NONE      ctermbg=NONE      ctermfg=243       gui=NONE      guibg=NONE      guifg=#767676    |    execute "highlight Comment cterm=" . g:monochrome_italic
hi String           cterm=NONE      ctermbg=NONE      ctermfg=067       gui=NONE      guibg=NONE      guifg=#5f87af
hi! def link Boolean                                  String
hi! def link Float                                    String
hi! def link Number                                   String
hi! def link Constant                                 String
hi! def link Identifier                               Normal
hi! def link Special                                  Normal
hi Function         cterm=NONE      ctermbg=NONE      ctermfg=015       gui=NONE      guibg=NONE      guifg=#ffffff
hi! def link Function                                 Function
hi! def link PreProc                                  Function
hi! def link Statement                                Function
hi! def link Type                                     Function
hi Todo             cterm=bold      ctermbg=196       ctermfg=bg        gui=bold      guibg=#ff0000   guifg=bg

" for Diary
hi diaryTodo        cterm=NONE      ctermbg=086       ctermfg=bg        gui=NONE      guibg=#5fffd7   guifg=bg
hi diarySets        cterm=NONE      ctermbg=NONE      ctermfg=066       gui=NONE      guibg=NONE      guifg=#5f8787
hi diaryPending     cterm=NONE      ctermbg=NONE      ctermfg=067       gui=NONE      guibg=NONE      guifg=#5c85b2
hi diaryNote        cterm=NONE      ctermbg=NONE      ctermfg=146       gui=NONE      guibg=NONE      guifg=#afafd7

" for Diff
hi diffFile         cterm=NONE      ctermbg=NONE      ctermfg=146       gui=NONE      guibg=NONE      guifg=#afafd7
hi diffNewFile      cterm=NONE      ctermbg=NONE      ctermfg=146       gui=NONE      guibg=NONE      guifg=#afafd7
hi diffLine         cterm=NONE      ctermbg=NONE      ctermfg=255       gui=NONE      guibg=NONE      guifg=#eeeeee
hi diffIndexLine    cterm=NONE      ctermbg=NONE      ctermfg=255       gui=NONE      guibg=NONE      guifg=#eeeeee
hi diffAdded        cterm=bold      ctermbg=NONE      ctermfg=121       gui=NONE      guibg=NONE      guifg=#87ffaf
hi! def link diffRemoved                              String

" for Diff mode
hi DiffDelete       cterm=NONE      ctermbg=067       ctermfg=bg        gui=NONE      guibg=#5c85b2   guifg=bg
hi DiffAdd          cterm=NONE      ctermbg=121       ctermfg=bg        gui=NONE      guibg=#87ffaf   guifg=bg
" hi DiffChange       cterm=NONE      ctermbg=067       ctermfg=bg        gui=NONE      guibg=#5c85b2   guifg=bg
" hi DiffText         cterm=NONE      ctermbg=121       ctermfg=bg        gui=NONE      guibg=#87ffaf   guifg=bg
hi DiffChange       cterm=NONE      ctermbg=146       ctermfg=bg      gui=NONE      guibg=#afafd7   guifg=bg
hi DiffText         cterm=NONE      ctermbg=015       ctermfg=bg      gui=NONE      guibg=#ffffff   guifg=bg

" for Help
hi helpSpecial      cterm=NONE      ctermbg=NONE      ctermfg=NONE      gui=NONE      guibg=NONE      guifg=NONE
hi helpNote         cterm=NONE      ctermbg=NONE      ctermfg=NONE      gui=NONE      guibg=NONE      guifg=NONE
hi! def link helpHyperTextJump                        String

" for HTML
hi htmlBold         cterm=bold      ctermbg=NONE      ctermfg=NONE      gui=bold      guibg=NONE      guifg=NONE
hi htmlItalic       cterm=NONE      ctermbg=NONE      ctermfg=NONE      gui=italic    guibg=NONE      guifg=NONE    |    execute "highlight htmlItalic cterm=" . g:monochrome_italic
hi htmlUnderline    cterm=underline ctermbg=NONE      ctermfg=NONE      gui=underline guibg=NONE      guifg=NONE
hi! def link htmlComment                              Comment
hi! def link htmlCommentPart                          Comment

" for Markdown
hi mkdBlockquote    cterm=NONE      ctermbg=NONE      ctermfg=246       gui=italic    guibg=NONE      guifg=#949494 |    execute "hi mkdBlockquote cterm=" . g:silence_italic
hi! def link markdownHeadingDelimiter                 Title
hi! def link markdownHeadingRule                      Title
hi! def link markdownLinkText                         Normal
hi! def link mkdCode                                  String
hi! def link mkdComment                               Comment
hi! def link mkdLink                                  String
hi! def link mkdURL                                   LineNr
hi! def link mkdInlineURL                             String

" for Mail
hi mailQuoted4      cterm=NONE      ctermbg=NONE      ctermfg=240       gui=italic    guibg=NONE      guifg=#585858 |    execute "hi mailQuoted4 cterm=" . g:silence_italic
hi mailQuoted3      cterm=NONE      ctermbg=NONE      ctermfg=242       gui=italic    guibg=NONE      guifg=#6c6c6c |    execute "hi mailQuoted3 cterm=" . g:silence_italic
hi mailQuoted2      cterm=NONE      ctermbg=NONE      ctermfg=244       gui=italic    guibg=NONE      guifg=#808080 |    execute "hi mailQuoted2 cterm=" . g:silence_italic
hi mailQuoted1      cterm=NONE      ctermbg=NONE      ctermfg=246       gui=italic    guibg=NONE      guifg=#949494 |    execute "hi mailQuoted1 cterm=" . g:silence_italic
hi! def link mailEmail                                String
hi! def link mailURL                                  String
hi! def link mailHeader                               mailQuoted3
hi! def link mailHeaderKey                            mailQuoted3
hi! def link mailSubject                              Title

" for Spelling
hi SpellBad         cterm=NONE      ctermbg=240       ctermfg=NONE      gui=NONE      guibg=#585858   guifg=NONE
hi SpellCap         cterm=NONE      ctermbg=240       ctermfg=NONE      gui=NONE      guibg=#585858   guifg=NONE
hi SpellLocal       cterm=NONE      ctermbg=240       ctermfg=NONE      gui=NONE      guibg=#585858   guifg=NONE
hi SpellRare        cterm=NONE      ctermbg=240       ctermfg=NONE      gui=NONE      guibg=#585858   guifg=NONE

" for Startify
hi! def link StartifyPath                             Comment
hi! def link StartifySection                          Title

" for Vim
hi vimCommentTitle  cterm=NONE      ctermbg=NONE      ctermfg=246       gui=NONE      guibg=NONE      guifg=#949494


" NOTES

" On shades of blue:
" the correct "macish" color should be #3a90ff (or #007aff / #007afe) but i seem to like #5f87ff more

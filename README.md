# Monolith colorscheme for Vim

## About

Monolith is a (not so) minimal colorscheme for Vim editor.

## Installation

Copy `monolith.vim` to the `$VIMHOME/colors` directory, or simply use any plugin manager available.

## Screenshots

C:

![c](https://i.imgur.com/n4F6vja.png)

Javascript:

![js](https://i.imgur.com/fDubfwM.png)

Perl:

![perl](https://i.imgur.com/tfMvIaK.png)

Python:

![python](https://i.imgur.com/Df80lSG.png)

R:
![r](https://i.imgur.com/7wzzDCE.png)

Diff:

![diff](https://i.imgur.com/gXYuKoj.png)

Markdown:

![MKD](https://i.imgur.com/bm6lL4X.png)

Vim:

![vim](https://i.imgur.com/JPUcwRt.png)
